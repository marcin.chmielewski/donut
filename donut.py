import bpy
import math

class InvalidVerticesAmount(Exception):
    
    def __init__(self,msg):
        super().__init__(msg)

class Object():
    
    def __init__(self,mesh):
        self.obj = bpy.data.objects.new('new_object',mesh)
        
class donut:
    
    def __init__(self,N):
        self.edges = []
        self.faces = []
        self.vertices = list()
        self.N = N
        if (self.N <= 0):
            raise InvalidVerticesAmount(N)
        
    def create_vertices(self):
        for i in range(8):
            x = math.sin(math.pi*2*i/8)+10
            y = 0 
            z = math.cos(math.pi*2*i/8)
            for angle in [math.pi*2*i/self.N for i in range(self.N)]:
                new_x = math.cos(angle) * x - math.sin(angle) * y
                new_y = math.sin(angle) * x + math.cos(angle) * y
                new_z = z
                tmp = (new_x, new_y, new_z)
                self.vertices.append(tmp)
        
    def create_faces(self):
        N = self.N
        for i in range((self.N*8)-N-1):
            self.faces.append((i,i+1,i+N))
            self.faces.append((i+1,i+N,i+N+1))
        for i in range(self.N-1):
            self.faces.append((i+1,(N*7)+i,(N*7)+i+1))
            self.faces.append((i,i+1,(N*7)+i))
        self.faces.append((0,N,N-1))
        self.faces.append((0,N-1,(N*7)))
        


tst = donut(50)
tst.create_vertices()
tst.create_faces()

new_mesh = bpy.data.meshes.new('new_mesh')
new_mesh.from_pydata(tst.vertices, tst.edges, tst.faces)
new_mesh.update()
new_object = Object(new_mesh)
new_collection = bpy.data.collections.new('new_collection')
bpy.context.scene.collection.children.link(new_collection)
new_collection.objects.link(new_object.obj)
